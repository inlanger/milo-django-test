import datetime

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from main.models import Profile


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile


class ExtendedUserEditForm(forms.ModelForm):
    birth_date = forms.DateField(help_text='YYYY-MM-DD')

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'birth_date']

    def __init__(self, *args, **kwargs):
        super(ExtendedUserEditForm, self).__init__(*args, **kwargs)
        user = kwargs['instance']
        self.fields['birth_date'].initial = user.profile.birth_date

    def save(self, commit=True):
        if not commit:
            raise NotImplementedError("Can't edit User and Profile without database save")
        user = super(ExtendedUserEditForm, self).save(commit=True)
        user_profile = Profile.objects.get(user=user)
        user_profile.birth_date = self.cleaned_data['birth_date']
        user_profile.save()
        return user


class ExtendedUserCreationForm(UserCreationForm):
    birth_date = forms.DateField(initial=datetime.datetime.now())

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'birth_date']

    def save(self, commit=True):
        if not commit:
            raise NotImplementedError("Can't create User and Profile without database save")
        user = super(ExtendedUserCreationForm, self).save(commit=True)
        user_profile = Profile(user=user, birth_date=self.cleaned_data['birth_date'])
        user_profile.save()
        print user_profile.birth_date
        return user
from random import randint

from django.db import models
from django.contrib.auth.models import User


def gen_random_number():
    return randint(1, 100)


class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    birth_date = models.DateField(null=True, blank=True)
    random_number = models.IntegerField(default=gen_random_number)
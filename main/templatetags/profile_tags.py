import datetime

from django import template


register = template.Library()


@register.simple_tag
def is_allowed(user):
    if user.profile.birth_date:
        if (datetime.date.today() - user.profile.birth_date).days/365 > 13:
            return 'allowed'
    return 'blocked'


@register.simple_tag
def bizz_or_fuzz(user):
    number = user.profile.random_number
    if number % 5 == 0 and number % 3 == 0:
        return 'BizzFuzz'
    elif number % 5 == 0:
        return 'Bizz'
    elif number % 3 == 0:
        return 'Fuzz'
    else:
        return number
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'main.views.users_list', name='index'),
    url(r'^create/$', 'main.views.create_user', name='create'),
    url(r'^view/(?P<pk>\d+)/$', 'main.views.single_user', name='single'),
    url(r'^edit/(?P<pk>\d+)/$', 'main.views.edit_user', name='edit'),
    url(r'^delete/(?P<pk>\d+)/$', 'main.views.delete_user', name='delete'),
    url(r'^download/$', 'main.views.download', name='download'),
)

if settings.DEBUG:
    urlpatterns = urlpatterns + patterns('',
        (r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )